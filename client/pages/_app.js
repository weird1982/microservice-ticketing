import 'bootstrap/dist/css/bootstrap.css';

const Wrapper = ({Component, pageProops}) => {
    return <Component {...pageProops}></Component>
}

export default Wrapper