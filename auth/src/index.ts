import mongoose from "mongoose"
import { app } from "./app";

const start = async () =>{
    if(!process.env.JWT_KEY){
        throw new Error('No JWT_KEY env')
    }
    try{
        await mongoose.connect('mongodb://mongo-0.mongo,mongo-1.mongo:27017/auth\_?')
        console.log('Connected to MongoDb')
    }
    catch(err){
        console.error(err)
    }

    app.listen(3000,()=>{
        console.log('Listening on port 3000')
    })
};

start();

