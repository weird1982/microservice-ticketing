import request from "supertest"
import { app } from "../../app"

it('returns a 201 on successful signup', async()=>{
    await request(app)
                .post('/api/users/signup')
                .send({
                    email: 'fgsds@mail.ru',
                    password:'123456'
                })
                .expect(201);
})

it('returns 400 with invalid email',async ()=>{
    await request(app)
                .post('/api/users/signup')
                .send({
                    email: 'fgsdsmail.ru',
                    password:'123456'
                })
                .expect(400);
})

it('returns 400 with invalid password',async ()=>{
    await request(app)
                .post('/api/users/signup')
                .send({
                    email: 'fgsds@mail.ru',
                    password:'1'
                })
                .expect(400);
})

it('returns 400 with missing email and password',async ()=>{
    await request(app)
                .post('/api/users/signup')
                .send({
                    password:'123456'
                })
                .expect(400);
    await request(app)
                .post('/api/users/signup')
                .send({
                    email:'fgsds@mail.ru'
                })
                .expect(400);
})

it('disallow duplicate email',async () =>{
    await request(app)
                .post('/api/users/signup')
                .send({
                    email: 'fgsds@mail.ru',
                    password:'123456'
                })
                .expect(201);
    await request(app)
                .post('/api/users/signup')
                .send({
                    email: 'fgsds@mail.ru',
                    password:'123456'
                })
                .expect(400);
})

it('sets cookie after succesfull signup', async () => {
    const response = await request(app)
                .post('/api/users/signup')
                .send({
                    email: 'fgsds@mail.ru',
                    password:'123456'
                })
                .expect(201);
            expect(response.get('Set-Cookie')).toBeDefined()
    
})