import request from "supertest"
import { app } from "../../app"

it('clean cookie after signout',async ()=>{
    await request(app)
           .post('/api/users/signup')
           .send({
               email:'xynta2000@mail.ru',
               password:"123456"
           })
           .expect(201)
    const response = await request(app)
           .post('/api/users/signout')
           .send({
           })
           .expect(200)
    expect(response.get('Set-Cookie')).toBeDefined()

})