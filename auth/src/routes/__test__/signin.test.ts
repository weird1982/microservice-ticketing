import request from "supertest"
import { app } from "../../app"

it('fali when email does not exusts',async ()=>{
    await request(app)
           .post('/api/users/signin')
           .send({
               email:'xynta2000@mail.ru',
               password:"123456"
           })
           .expect(400)
})

it('fali when incorrect password',async ()=>{
    await request(app)
           .post('/api/users/signup')
           .send({
               email:'xynta2000@mail.ru',
               password:"123456"
           })
           .expect(201)
    await request(app)
           .post('/api/users/signin')
           .send({
               email:'xynta2000@mail.ru',
               password:"12345"
           })
           .expect(400)
})

it('response with cookie when valid creds',async ()=>{
    await request(app)
           .post('/api/users/signup')
           .send({
               email:'xynta2000@mail.ru',
               password:"123456"
           })
           .expect(201)
    const response = await request(app)
           .post('/api/users/signin')
           .send({
               email:'xynta2000@mail.ru',
               password:"123456"
           })
           .expect(200)
    expect(response.get('Set-Cookie')).toBeDefined()

})